#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    char *hsh;
    // Hash the guess using MD5
    hsh = md5(guess, strlen(guess));
    //printf("%s\n", hsh);
    // Compare the two hashes
   // printf("HASH: %s \n hsh:  %s \n", hash, hsh);
    if (strncmp(hsh, hash, HASH_LEN) == 0)
    {
 
        free(hsh);
        return 1;
    } else 
    {
        free(hsh);
        return 0;
    }
}
// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
    //get the size
    struct stat info;
    stat(filename, &info);
    int file_len = info.st_size;
    
    //DEBUG
   // printf("Size: %d\n", file_len);
        //alloc some space
    char *chunk = (char *)malloc(file_len+1);
    
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        perror("can't open that file");
        exit(1);
    }
    fread(chunk, 1, file_len, f);
    fclose(f);
    
    chunk[file_len] = '\0';
    
    int lines = 0;
    for (int i = 0; i < file_len; i++)
    {
        if (chunk[i] == '\n')
        {
            chunk[i] = '\0';
            lines++;
        }
    }
    if (chunk[file_len-1] != '\0') lines++;

    char **strings = malloc((lines+1) * sizeof(char *));

    int string_idx = 0;
    for (int i = 0; i < file_len; i += strlen(chunk+i) + 1)
    {
        strings[string_idx] = chunk+i;
        string_idx++;
    }
    

    
    return strings;
    free(chunk);
    free(strings);

    
    

}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{
    //get the size
    struct stat info;
    stat(filename, &info);
    int file_len = info.st_size;
    
    //DEBUG
   // printf("Size: %d\n", file_len);
        //alloc some space
    char *chunk = (char *)malloc(file_len+1);
    
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        perror("can't open that file");
        exit(1);
    }
    fread(chunk, 1, file_len, f);
    fclose(f);
    
    chunk[file_len] = '\0';
    
    int lines = 0;
    for (int i = 0; i < file_len; i++)
    {
        if (chunk[i] == '\n')
        {
            chunk[i] = '\0';
            lines++;
        }
    }
    if (chunk[file_len-1] != '\0') lines++;
    
    //lets make  a separate array of the hashes and then lets join em
    
    
    char **strings = malloc((lines+1) * sizeof(char *) * 2 + (lines+1));
    char **hashens = malloc((lines+1) * sizeof(char *) * 2 + (lines+1));
    char **total = malloc((lines+1) * sizeof(char *) * 2 + (lines+1));

    int string_idx = 0;
    int hash_idx = 1;
    for (int i = 0; i <  file_len; i += strlen(chunk+i) + 1)
    {
        
        strings[string_idx] = chunk+i;
       // strcat(strings[string_idx], ":");
        hashens[hash_idx] = md5(chunk+i, strlen(chunk+i));
        //string_idx++;
        total[hash_idx] =  md5(hashens[hash_idx], strlen(hashens[hash_idx]));
        total[string_idx] = strings[string_idx];
       // strcat(total[string_idx], strings[string_idx]);
        //printf("%s\n", total[string_idx]);
        
        string_idx+=2;
        hash_idx+=2;
    
    }
    
    
    
    return total;
    free(strings);
    free(chunk);
    
   
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);

    int x = 0;
    int y = 0;
    
    while(x < 10)
    {

        printf("%s\n", dict[x]);
        x++;
    }
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    //qsort(dict, ___, ___, ___);

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
}
